package dev.team2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import dev.team2.models.*;

import dev.team2.util.ConnectionUtil;
import dev.team2.util.MapperUtil;

public class App {
    public static String junitTest() {
        return "hello world";
    }

    public static void main(String[] args){
		TapewORM tapeworm = new TapewORM();
		Email testEmail = new Email(1, "bruh");
		
		//boolean add = tapeworm.add("email_table", 1, "bruh");
		System.out.println("test add (completed)");
		//System.out.println(add);
		
		//boolean add2 = tapeworm.add(testEmail);
		System.out.println("test add2 (completed)");
		//System.out.println(add2);
		
		//boolean addDefault = tapeworm.addDefault("email_table", "bruh");
		System.out.println("test addDefault");
		//System.out.println(addDefault);
		
		//boolean update = tapeworm.update("email_table", "email", "bruhhuh", "emailid", 1);
		System.out.println("test update (completed)");
		//System.out.println(update);
		
		//boolean update2 = tapeworm.update(testEmail);
		System.out.println("test update2 (completed)");
		//System.out.println(update2);
		
		//List<Email> reply = tapeworm.get("email_table", "emailid", 9);
		System.out.println("test get (completed)");
		//System.out.println(reply);
		
		//List<Email> reply2 = tapeworm.getColumns("email_table", "email", "wyatt@goettsch.com", "emailid", "email");
		System.out.println("test getColumns (completed)");
		//System.out.println(reply2);
		
		//List<Email> reply3 = tapeworm.getNoWhere("email_table", "emailid", "email");
		System.out.println("test getNoWhere (completed)");
		//System.out.println(reply3);
		
		//List<Email> all = tapeworm.getAll("identity_table");
		System.out.println("test getAll (completed)");
		//System.out.println(all);
		
		//boolean delete = tapeworm.delete("email_table", "emailid", 1);
		System.out.println("test delete (completed)");
		//System.out.println(delete);
		
		//boolean delete2 = tapeworm.delete(testEmail);
		System.out.println("test delete2 (completed)");
		//System.out.println(delete2);
	}
/*
	public void get(int id) {
		String sql = "select id, email from email_table where id = ?";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getInt("id") + ": " + rs.getString("email"));
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}

	}
	*/
}
