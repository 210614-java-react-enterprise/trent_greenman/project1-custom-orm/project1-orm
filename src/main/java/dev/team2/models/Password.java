package dev.team2.models;

import java.util.*;

public class Password {
	private long emailid;
	private long passwordid;
	private String password;
	
	
	public Password() {
		super();
	}
	
	public Password(long emailid, long passwordid, String password) {
		super();
		this.emailid = emailid;
		this.passwordid = passwordid;
		this.password = password;
	}

	public long getEmailid() {
		return emailid;
	}

	public void setEmailid(long emailid) {
		this.emailid = emailid;
	}

	public long getPasswordid() {
		return passwordid;
	}

	public void setPasswordid(long passwordid) {
		this.passwordid = passwordid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "\nPassword{\n"
				+ "emailid = " + emailid + '\n'
				+ "passwordid = " + passwordid + '\n'
				+ "password = " + password + '\n'
				+ "}";
	}
}
