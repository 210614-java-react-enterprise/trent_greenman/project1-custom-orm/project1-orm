package dev.team2.models;

import java.util.*;

public class Email {
	private long emailid;
	
	private String email;
	
	public Email() {
		super();
	}
	
	public Email(long emailid, String email) {
		super();
		this.emailid = emailid;
		this.email = email;
	}
	
	public void setEmailid(long emailid) {
		this.emailid = emailid;
	}

	public long getEmailid() {
		return emailid;
	}

	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		return "\nSubscriber{\n"
				+ "emailid = " + emailid + '\n'
				+ "email = " + email
				+ "\n}";
	}
}
