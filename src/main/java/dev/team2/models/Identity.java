package dev.team2.models;

import java.time.LocalDate;
import java.util.*;

public class Identity {
	private long identityid;
	private long emailid;
	private String lastname;
	private String firstname;
	private String address;
	private String phone;
	
	public Identity() {
		super();
	}

	public Identity(long identityid, long emailid, String lastname, String firstname,
			String address, String phone) {
		super();
		this.identityid = identityid;
		this.emailid = emailid;
		this.lastname = lastname;
		this.firstname = firstname;
		this.address = address;
		this.phone = phone;
	}

	public long getIdentityid() {
		return identityid;
	}

	public void setIdentityid(long identityid) {
		this.identityid = identityid;
	}

	public long getEmailid() {
		return emailid;
	}

	public void setEmailid(long emailid) {
		this.emailid = emailid;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "\nIdentity{\n"
				+ "emailid = " + emailid + '\n'
				+ "identityid = " + identityid + '\n'
				+ "lastname = " + lastname + '\n'
				+ "firstname = " + firstname + '\n'
				+ "address = " + address + '\n'
				+ "phone = " + phone + '\n'
				+ "}";
	}
}
