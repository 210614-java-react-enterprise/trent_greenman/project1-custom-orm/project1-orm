package dev.team2.models;

public class Admin {
	private long adminid;
	private long emailid;
	public Admin() {
		super();
	}
	
	public Admin(long adminid, long emailid) {
		super();
		this.adminid = adminid;
		this.emailid = emailid;
	}

	public long getAdminid() {
		return adminid;
	}

	public void setAdminid(long adminid) {
		this.adminid = adminid;
	}

	public long getEmailid() {
		return emailid;
	}

	public void setEmailid(long emailid) {
		this.emailid = emailid;
	}
	
	@Override
	public String toString() {
		return "\nAdmin{\n"
				+ "adminid = " + adminid + '\n'
				+ "emailid = " + emailid
				+ "\n}";
	}
}
